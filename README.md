# bert-paper-analysis
+ 原始论文：Bert Pre-training of Deep Bidirectional Transformers for Language Understanding.pdf
+ 阅读记录：bert论文阅读记录
+ 程序代码：bert_fine_tune
    * main_classifier_fine_tune.py是分类执行文件。
    * chinese_L-12_H-768_A-12/中的预训练模型，需要从https://storage.googleapis.com/bert_models/2018_11_03/chinese_L-12_H-768_A-12.zip下载。
